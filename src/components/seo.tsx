import React from 'react'
import {useStaticQuery, graphql} from "gatsby"
import {Helmet} from "react-helmet"
 
function Seo() {
    const data = useStaticQuery(graphql`
        {
            allContentfulSiteMetadata {
                nodes {
                    siteName
                    siteDescription
                }
            }
        }
  `)
    

    const {siteName, siteDescription} = data.allContentfulSiteMetadata.nodes[0]

   
    return (
        <div>
            <Helmet>
                <title>{siteName}</title>
                <meta name="description" content={siteDescription}/>
            </Helmet>
        </div>
    )
}

export default Seo